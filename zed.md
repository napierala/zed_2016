# zed

Data: 2016-01-18 00:26:44



### Podsumowanie analizy zbioru danych

Etap przygotowania danych zawiera usuwanie wielu kolumn, ze względu na ich zawartośc(w pełni wypełnione wartościami NA). Ogromna korelacja pomiędzy wartościami atomów i elektronów zamodelowanych w pliku oraz wyliczonych z tablicy pierwiastów została odkryta. Patrząc na wykres liczby elektronów a liczby atomów widać, że największe skupienie istnieje w niższych przedziałach. Po usunięciu jakiś wierszy czy kolumn należy zresetować poziomy(levels) naszych danych w szczególności res_name gdyż na jego podstawie będzie budowany klasyfikator.

### Użyte biblioteki:


```r
library(EDAWR)
library(knitr)
library(dplyr)
library(MASS)
library(ggplot2)
library(RColorBrewer)
library(quantregForest)
library(caret)
```

### Powtarzalność eksperymentów:


```r
set.seed(26);
```

### Wczytywanie danych:


```r
data <- read.csv("all_summary.txt", comment.char = "#", head=TRUE,sep=";", nrows = 1000)
```

### Przygotowywanie danych:


```r
valuesToFilter <- c("DA","DC","DT", "DU", "DG", "DI","UNK", "UNX", "UNL", "PR", "PD", "Y1", "EU", "N", "15P", "UQ", "PX4", "NAN")

data = data %>% dplyr::filter(!(res_name %in% valuesToFilter)) %>% dplyr::filter(!is.na(res_name))
```


```r
# New factors are needed as the levels for res_name are still from the original data
data <- data %>% mutate(res_name=factor(paste("CLASS_",res_name,sep="")))
```


```r
data = data %>% dplyr::group_by(pdb_code, res_name) %>% dplyr::filter(row_number() == 1)
```



```r
deleteColumnsById <- function(columnsIdsToDelete) {
  data <<- dplyr::select(data, -columnsIdsToDelete)
}

deleteColumnsByName <- function(columnNamesToDelete) {
  colNums <- match(columnNamesToDelete,colnames(data))
  deleteColumnsById(colNums)
}

# Get Rid of the not needed columns
deleteColumnsByName(c("local_BAa", "local_NPa", "local_Ra", "local_RGa", "local_SRGa", "local_CCSa",   "local_CCPa", "local_ZOa", "local_ZDa", "local_ZD_minus_a", "local_ZD_plus_a"))

# Get rid of the part_XX columns(except for part_01)
deleteColumnsById(grep("part_00", colnames(data), fixed = TRUE))
deleteColumnsById(grep("part_02", colnames(data), fixed = TRUE))
deleteColumnsById(grep("part_03", colnames(data), fixed = TRUE))
deleteColumnsById(grep("part_04", colnames(data), fixed = TRUE))
deleteColumnsById(grep("part_05", colnames(data), fixed = TRUE))
deleteColumnsById(grep("part_06", colnames(data), fixed = TRUE))
deleteColumnsById(grep("part_07", colnames(data), fixed = TRUE))
deleteColumnsById(grep("part_08", colnames(data), fixed = TRUE))
deleteColumnsById(grep("part_09", colnames(data), fixed = TRUE))
```


```r
#data = data %>% dplyr::filter(!is.na(local_res_atom_non_h_count)) %>% dplyr::filter(!is.na( dict_atom_non_h_count)) %>% dplyr::filter(!is.na( local_res_atom_non_h_electron_sum)) %>% dplyr::filter(!is.na( dict_atom_non_h_electron_sum)) %>% dplyr::filter(!is.na( local_res_atom_C_count)) %>% dplyr::filter(!is.na( dict_atom_C_count)) %>% dplyr::filter(!is.na( local_res_atom_N_count)) %>% dplyr::filter(!is.na( dict_atom_N_count)) %>% dplyr::filter(!is.na( local_res_atom_O_count)) %>% dplyr::filter(!is.na( dict_atom_O_count)) %>% dplyr::filter(!is.na( local_res_atom_S_count)) %>% dplyr::filter(!is.na( dict_atom_S_count))
```

### Podsumowanie wartości w każdej kolumnie:


                title        pdb_code      res_name       res_id          chain_id   local_res_atom_count   local_res_atom_non_h_count   local_res_atom_non_h_occupancy_sum   local_res_atom_non_h_electron_sum   local_res_atom_non_h_electron_occupancy_sum   local_res_atom_C_count   local_res_atom_N_count   local_res_atom_O_count   local_res_atom_S_count   dict_atom_non_h_count   dict_atom_non_h_electron_sum   dict_atom_C_count   dict_atom_N_count   dict_atom_O_count   dict_atom_S_count   part_01_blob_electron_sum   part_01_blob_volume_sum   part_01_blob_parts   part_01_shape_O3   part_01_shape_O4    part_01_shape_O5    part_01_shape_FL    part_01_shape_O3_norm   part_01_shape_O4_norm   part_01_shape_O5_norm   part_01_shape_FL_norm   part_01_shape_I1    part_01_shape_I2    part_01_shape_I3    part_01_shape_I4    part_01_shape_I5    part_01_shape_I6    part_01_shape_I1_norm   part_01_shape_I2_norm   part_01_shape_I3_norm   part_01_shape_I4_norm   part_01_shape_I5_norm   part_01_shape_I6_norm   part_01_shape_I1_scaled   part_01_shape_I2_scaled   part_01_shape_I3_scaled   part_01_shape_I4_scaled   part_01_shape_I5_scaled   part_01_shape_I6_scaled   part_01_shape_M000   part_01_shape_E3_E1   part_01_shape_E2_E1   part_01_shape_E3_E2   part_01_shape_sqrt_E1   part_01_shape_sqrt_E2   part_01_shape_sqrt_E3   part_01_density_O3   part_01_density_O4   part_01_density_O5   part_01_density_FL   part_01_density_O3_norm   part_01_density_O4_norm   part_01_density_O5_norm   part_01_density_FL_norm   part_01_density_I1   part_01_density_I2   part_01_density_I3   part_01_density_I4   part_01_density_I5   part_01_density_I6   part_01_density_I1_norm   part_01_density_I2_norm   part_01_density_I3_norm   part_01_density_I4_norm   part_01_density_I5_norm   part_01_density_I6_norm   part_01_density_I1_scaled   part_01_density_I2_scaled   part_01_density_I3_scaled   part_01_density_I4_scaled   part_01_density_I5_scaled   part_01_density_I6_scaled   part_01_density_M000   part_01_density_E3_E1   part_01_density_E2_E1   part_01_density_E3_E2   part_01_density_sqrt_E1   part_01_density_sqrt_E2   part_01_density_sqrt_E3    local_volume    local_electrons     local_mean        local_std        local_min     local_max      local_skewness    local_parts        fo_col        fc_col    weight_col       grid_space   solvent_radius   solvent_opening_radius   resolution_max_limit     resolution     TwoFoFc_mean         TwoFoFc_std      TwoFoFc_square_std    TwoFoFc_min       TwoFoFc_max        Fo_mean               Fo_std        Fo_square_std         Fo_min            Fo_max         FoFc_mean             FoFc_std       FoFc_square_std       FoFc_min          FoFc_max         Fc_mean               Fc_std        Fc_square_std         Fc_min            Fc_max       solvent_mask_count   void_mask_count   modeled_mask_count   solvent_ratio    TwoFoFc_bulk_mean   TwoFoFc_bulk_std   TwoFoFc_void_mean   TwoFoFc_void_std   TwoFoFc_modeled_mean   TwoFoFc_modeled_std    Fo_bulk_mean        Fo_bulk_std       Fo_void_mean      Fo_void_std     Fo_modeled_mean   Fo_modeled_std   FoFc_bulk_mean       FoFc_bulk_std     FoFc_void_mean      FoFc_void_std     FoFc_modeled_mean   FoFc_modeled_std    Fc_bulk_mean         Fc_bulk_std       Fc_void_mean       Fc_void_std      Fc_modeled_mean   Fc_modeled_std   TwoFoFc_void_fit_binormal_mean1   TwoFoFc_void_fit_binormal_std1   TwoFoFc_void_fit_binormal_mean2   TwoFoFc_void_fit_binormal_std2   TwoFoFc_void_fit_binormal_scale   TwoFoFc_solvent_fit_normal_mean   TwoFoFc_solvent_fit_normal_std   part_step_FoFc_std_min   part_step_FoFc_std_max   part_step_FoFc_std_step 
---  -------------------  ------------  ------------  ---------------  ------------  ---------------------  ---------------------------  -----------------------------------  ----------------------------------  --------------------------------------------  -----------------------  -----------------------  -----------------------  -----------------------  ----------------------  -----------------------------  ------------------  ------------------  ------------------  ------------------  --------------------------  ------------------------  -------------------  -----------------  ------------------  ------------------  ------------------  ----------------------  ----------------------  ----------------------  ----------------------  ------------------  ------------------  ------------------  ------------------  ------------------  ------------------  ----------------------  ----------------------  ----------------------  ----------------------  ----------------------  ----------------------  ------------------------  ------------------------  ------------------------  ------------------------  ------------------------  ------------------------  -------------------  --------------------  --------------------  --------------------  ----------------------  ----------------------  ----------------------  -------------------  -------------------  -------------------  -------------------  ------------------------  ------------------------  ------------------------  ------------------------  -------------------  -------------------  -------------------  -------------------  -------------------  -------------------  ------------------------  ------------------------  ------------------------  ------------------------  ------------------------  ------------------------  --------------------------  --------------------------  --------------------------  --------------------------  --------------------------  --------------------------  ---------------------  ----------------------  ----------------------  ----------------------  ------------------------  ------------------------  ------------------------  ---------------  ----------------  ----------------  ---------------  ------------  ---------------  ---------------  ---------------  -----------  ------------  -------------  -------------  ---------------  -----------------------  ---------------------  --------------  -------------------  ----------------  -------------------  ----------------  ---------------  -------------------  ----------------  ----------------  ----------------  ---------------  -------------------  ----------------  -----------------  ----------------  ---------------  -------------------  ----------------  ----------------  ----------------  ---------------  -------------------  ----------------  -------------------  ---------------  ------------------  -----------------  ------------------  -----------------  ---------------------  --------------------  ------------------  ----------------  ----------------  ---------------  ----------------  ---------------  -------------------  ----------------  ------------------  ----------------  ------------------  -----------------  -------------------  ----------------  -----------------  ----------------  ----------------  ---------------  --------------------------------  -------------------------------  --------------------------------  -------------------------------  --------------------------------  --------------------------------  -------------------------------  -----------------------  -----------------------  ------------------------
     1amp ZN 502 A :  1   2ah9   :  5   SO4    : 33   Min.   :  -4.0   A      :254   Min.   : 1.00          Min.   : 1.00                Min.   : 0.40                        Min.   :  7.0                       Min.   :  7.00                                Min.   : 0.000           Min.   : 0.000           Min.   : 0.000           Min.   :0.0000           Min.   : 1.00           Min.   :  7.0                  Min.   : 0.000      Min.   : 0.00       Min.   : 0.000      Min.   :0.0000      Min.   :  0.000             Min.   :  0.00            Min.   :0.0000       Min.   :   25567   Min.   :2.147e+08   Min.   :5.413e+11   Min.   :2.798e+07   Min.   :0.2317          Min.   :0.01788         Min.   :0.000459        Min.   :0.000017        Min.   :7.446e+05   Min.   :1.445e+11   Min.   :1.165e+11   Min.   :1.137e+07   Min.   :1.349e+04   Min.   :6.446e+09   Min.   : 0.06411        Min.   : 0.001093       Min.   :  0.00083       Min.   :0.000007        Min.   :0.000000        Min.   : 0.004967       Min.   :0.000221          Min.   :0.0e+00           Min.   :0.000000          Min.   :0.0e+00           Min.   :0.000000          Min.   :0e+00             Min.   : 1031        Min.   :0.01210       Min.   :0.03362       Min.   :0.04965       Min.   : 2.988          Min.   : 2.146          Min.   :1.693           Min.   :    8116     Min.   :1.588e+07    Min.   :8.926e+09    Min.   :6.972e+06    Min.   :0.08596           Min.   :0.002427          Min.   :0.000023          Min.   :0.000002          Min.   :3.540e+05    Min.   :1.370e+10    Min.   :3.467e+10    Min.   :3.337e+06    Min.   :1.638e+05    Min.   :1.398e+09    Min.   : 0.01029          Min.   : 0.000028         Min.   :  0.00002         Min.   :0.000001          Min.   :0.000000          Min.   : 0.000303         Min.   :0.000035            Min.   :0.000000            Min.   :0.000000            Min.   :0.000000            Min.   :0.000000            Min.   :0.0e+00             Min.   :  266.6        Min.   :0.01234         Min.   :0.03117         Min.   :0.05098         Min.   : 2.652            Min.   : 2.068            Min.   :1.651             Min.   : 106.7   Min.   :  0.000   Min.   :0.00000   Min.   :0.0000   Min.   :0     Min.   :0.0000   Min.   :0.0000   Min.   :0.0000   DELFWT:393   PHDELWT:393   Mode:logical   Min.   :0.2    Min.   :1.9      Min.   :1.4              Min.   :2              Min.   :1.000   Min.   :-6.876e-09   Min.   :0.09003   Min.   :0.0157       Min.   :-1.8676   Min.   :0.4834   Min.   :-1.560e-08   Min.   :0.08983   Min.   :0.01938   Min.   :-1.8088   Min.   :0.5448   Min.   :-3.786e-09   Min.   :0.05147   Min.   :0.003944   Min.   :-2.0514   Min.   :0.3198   Min.   :-4.275e-09   Min.   :0.06435   Min.   :0.01079   Min.   :-1.5119   Min.   :0.4599   Min.   :       0     Min.   :  34269   Min.   :  81927      Min.   :0.0000   Min.   :-0.043130   Min.   :0.05045    Min.   :-0.25192    Min.   :0.07756    Min.   :0.02241        Min.   :0.1188        Min.   :-0.034353   Min.   :0.03167   Min.   :-0.2283   Min.   :0.0770   Min.   :0.01726   Min.   :0.1283   Min.   :-0.0236569   Min.   :0.04863   Min.   :-0.055127   Min.   :0.05722   Min.   :-0.01619    Min.   :0.05443    Min.   :-0.0254483   Min.   :0.01881   Min.   :-0.19679   Min.   :0.04343   Min.   :0.01079   Min.   :0.0870   Min.   :-0.45490                  Min.   :0.06743                  Min.   :-0.13518                  Min.   :0.05152                  Min.   :0.09765                   Min.   :-0.0429162                Min.   :0.04795                  Min.   :2.5              Min.   :7.1              Min.   :0.5             
     1b39 ATP 381 A:  1   2z2t   :  5   CA     : 27   1st Qu.: 301.0   B      : 77   1st Qu.: 1.00          1st Qu.: 1.00                1st Qu.: 1.00                        1st Qu.: 29.0                       1st Qu.: 28.00                                1st Qu.: 0.000           1st Qu.: 0.000           1st Qu.: 0.000           1st Qu.:0.0000           1st Qu.: 1.00           1st Qu.: 29.0                  1st Qu.: 0.000      1st Qu.: 0.00       1st Qu.: 0.000      1st Qu.:0.0000      1st Qu.:  8.414             1st Qu.: 13.02            1st Qu.:1.0000       1st Qu.:  102608   1st Qu.:2.631e+09   1st Qu.:1.752e+13   1st Qu.:4.634e+10   1st Qu.:0.3249          1st Qu.:0.02785         1st Qu.:0.000668        1st Qu.:0.002226        1st Qu.:8.123e+06   1st Qu.:1.198e+13   1st Qu.:2.599e+13   1st Qu.:2.525e+10   1st Qu.:5.669e+09   1st Qu.:3.801e+11   1st Qu.: 0.15470        1st Qu.: 0.004162       1st Qu.:  0.00964       1st Qu.:0.001198        1st Qu.:0.000276        1st Qu.: 0.022728       1st Qu.:0.000827          1st Qu.:0.0e+00           1st Qu.:0.000000          1st Qu.:0.0e+00           1st Qu.:0.000000          1st Qu.:0e+00             1st Qu.: 1845        1st Qu.:0.06484       1st Qu.:0.17656       1st Qu.:0.29874       1st Qu.: 5.740          1st Qu.: 3.545          1st Qu.:2.569           1st Qu.:   62574     1st Qu.:9.507e+08    1st Qu.:4.116e+12    1st Qu.:1.625e+10    1st Qu.:0.36500           1st Qu.:0.036818          1st Qu.:0.001088          1st Qu.:0.003278          1st Qu.:4.516e+06    1st Qu.:3.377e+12    1st Qu.:7.826e+12    1st Qu.:8.504e+09    1st Qu.:1.959e+09    1st Qu.:1.294e+11    1st Qu.: 0.21368          1st Qu.: 0.007739         1st Qu.:  0.01617         1st Qu.:0.002242          1st Qu.:0.000969          1st Qu.: 0.033928         1st Qu.:0.001492            1st Qu.:0.000000            1st Qu.:0.000001            1st Qu.:0.000001            1st Qu.:0.000000            1st Qu.:0.0e+00             1st Qu.: 1281.8        1st Qu.:0.06199         1st Qu.:0.16963         1st Qu.:0.30222         1st Qu.: 5.178            1st Qu.: 3.343            1st Qu.:2.414             1st Qu.: 235.9   1st Qu.:  9.517   1st Qu.:0.02462   1st Qu.:0.1123   1st Qu.:0     1st Qu.:0.8425   1st Qu.:0.1794   1st Qu.:1.0000   NA           NA            NA's:393       1st Qu.:0.2    1st Qu.:1.9      1st Qu.:1.4              1st Qu.:2              1st Qu.:1.762   1st Qu.:-8.830e-11   1st Qu.:0.21587   1st Qu.:0.1077       1st Qu.:-1.0800   1st Qu.:1.6897   1st Qu.:-8.030e-11   1st Qu.:0.20683   1st Qu.:0.10853   1st Qu.:-1.0441   1st Qu.:1.6630   1st Qu.:-6.950e-11   1st Qu.:0.10255   1st Qu.:0.023502   1st Qu.:-0.8957   1st Qu.:1.1837   1st Qu.:-6.519e-11   1st Qu.:0.19428   1st Qu.:0.09696   1st Qu.:-0.9075   1st Qu.:1.5816   1st Qu.:  307428     1st Qu.: 302589   1st Qu.: 498436      1st Qu.:0.2314   1st Qu.:-0.008562   1st Qu.:0.08679    1st Qu.:-0.16977    1st Qu.:0.16850    1st Qu.:0.07871        1st Qu.:0.2916        1st Qu.:-0.006276   1st Qu.:0.05700   1st Qu.:-0.1581   1st Qu.:0.1460   1st Qu.:0.07252   1st Qu.:0.2904   1st Qu.:-0.0036219   1st Qu.:0.08538   1st Qu.:-0.030206   1st Qu.:0.10349   1st Qu.: 0.00757    1st Qu.:0.11496    1st Qu.:-0.0055821   1st Qu.:0.03145   1st Qu.:-0.14522   1st Qu.:0.13042   1st Qu.:0.06651   1st Qu.:0.2665   1st Qu.:-0.32886                  1st Qu.:0.12473                  1st Qu.:-0.03420                  1st Qu.:0.10260                  1st Qu.:0.40150                   1st Qu.:-0.0078543                1st Qu.:0.08425                  1st Qu.:2.5              1st Qu.:7.1              1st Qu.:0.5             
     1cvl CA 320 A :  1   3ada   :  5   GOL    : 23   Median : 501.0   C      : 20   Median : 6.00          Median : 6.00                Median : 6.00                        Median : 48.0                       Median : 48.00                                Median : 3.000           Median : 0.000           Median : 3.000           Median :0.0000           Median : 6.00           Median : 48.0                  Median : 3.000      Median : 0.00       Median : 3.000      Median :0.0000      Median : 16.601             Median : 24.19            Median :1.0000       Median :  374513   Median :3.078e+10   Median :6.353e+14   Median :1.372e+12   Median :0.5023          Median :0.05379         Median :0.001429        Median :0.015796        Median :5.532e+07   Median :4.425e+14   Median :1.622e+15   Median :7.116e+11   Median :3.266e+11   Median :8.252e+12   Median : 0.38631        Median : 0.018458       Median :  0.07604       Median :0.010218        Median :0.003697        Median : 0.097724       Median :0.001667          Median :0.0e+00           Median :0.000001          Median :2.0e-06           Median :0.000001          Median :0e+00             Median : 3370        Median :0.12264       Median :0.30608       Median :0.47097       Median : 8.932          Median : 4.803          Median :3.228           Median :  175135     Median :7.438e+09    Median :7.677e+13    Median :4.873e+11    Median :0.67263           Median :0.095058          Median :0.003118          Median :0.038621          Median :2.638e+07    Median :9.551e+13    Median :3.941e+14    Median :3.048e+11    Median :1.748e+11    Median :2.001e+12    Median : 0.74496          Median : 0.059587         Median :  0.29159         Median :0.023366          Median :0.009841          Median : 0.253662         Median :0.003838            Median :0.000002            Median :0.000007            Median :0.000008            Median :0.000003            Median :0.0e+00             Median : 2240.0        Median :0.12174         Median :0.29963         Median :0.47951         Median : 8.452            Median : 4.510            Median :2.992             Median : 411.6   Median : 17.166   Median :0.03546   Median :0.1405   Median :0     Median :1.2319   Median :0.2398   Median :1.0000   NA           NA            NA             Median :0.2    Median :1.9      Median :1.4              Median :2              Median :2.000   Median : 1.045e-10   Median :0.26929   Median :0.1657       Median :-0.9282   Median :2.4079   Median : 7.540e-11   Median :0.26072   Median :0.17030   Median :-0.8420   Median :2.2935   Median : 1.329e-11   Median :0.12859   Median :0.037803   Median :-0.7139   Median :1.9085   Median : 1.494e-10   Median :0.24447   Median :0.15746   Median :-0.7219   Median :2.2456   Median :  592832     Median : 473632   Median : 854304      Median :0.3231   Median :-0.004130   Median :0.10923    Median :-0.14249    Median :0.19435    Median :0.08642        Median :0.3491        Median :-0.003145   Median :0.07127   Median :-0.1326   Median :0.1703   Median :0.08046   Median :0.3453   Median :-0.0015731   Median :0.10723   Median :-0.019216   Median :0.12921   Median : 0.01230    Median :0.14384    Median :-0.0024338   Median :0.03996   Median :-0.12228   Median :0.14846   Median :0.07068   Median :0.3343   Median :-0.30210                  Median :0.14370                  Median :-0.02653                  Median :0.13137                  Median :0.44298                   Median :-0.0032579                Median :0.10744                  Median :2.5              Median :7.1              Median :0.5             
     1db1 VDX 428 A:  1   4ahs   :  5   ZN     : 22   Mean   : 709.7   D      : 16   Mean   :13.65          Mean   :13.52                Mean   :13.08                        Mean   :101.5                       Mean   : 97.86                                Mean   : 7.377           Mean   : 1.397           Mean   : 3.891           Mean   :0.1476           Mean   :13.74           Mean   :103.2                  Mean   : 7.433      Mean   : 1.41       Mean   : 4.025      Mean   :0.1501      Mean   : 28.742             Mean   : 44.54            Mean   :0.9542       Mean   : 2742145   Mean   :4.878e+12   Mean   :2.735e+18   Mean   :2.987e+15   Mean   :0.6248          Mean   :0.08449         Mean   :0.002678        Mean   :0.072528        Mean   :3.115e+09   Mean   :5.582e+18   Mean   :6.622e+19   Mean   :1.713e+15   Mean   :8.632e+14   Mean   :3.389e+16   Mean   : 0.87120        Mean   : 0.192241       Mean   :  1.97879       Mean   :0.048207        Mean   :0.031993        Mean   : 0.645186       Mean   :0.002707          Mean   :1.0e-06           Mean   :0.000014          Mean   :9.0e-06           Mean   :0.000006          Mean   :0e+00             Mean   : 6012        Mean   :0.20245       Mean   :0.38000       Mean   :0.49976       Mean   :11.263          Mean   : 5.599          Mean   :3.563           Mean   : 1625609     Mean   :1.677e+12    Mean   :5.018e+17    Mean   :1.183e+15    Mean   :0.81188           Mean   :0.147648          Mean   :0.006427          Mean   :0.231366          Mean   :1.846e+09    Mean   :1.916e+18    Mean   :2.344e+19    Mean   :7.715e+14    Mean   :4.970e+14    Mean   :1.186e+16    Mean   : 1.61905          Mean   : 0.588161         Mean   :  5.87861         Mean   :0.168924          Mean   :0.127297          Mean   : 1.590349         Mean   :0.008309            Mean   :0.000018            Mean   :0.000209            Mean   :0.000095            Mean   :0.000081            Mean   :1.0e-06             Mean   : 3878.9        Mean   :0.20607         Mean   :0.37961         Mean   :0.50425         Mean   :10.830            Mean   : 5.306            Mean   :3.359             Mean   : 951.2   Mean   : 30.511   Mean   :0.04250   Mean   :0.1749   Mean   :0     Mean   :1.6471   Mean   :0.2942   Mean   :0.9695   NA           NA            NA             Mean   :0.2    Mean   :1.9      Mean   :1.4              Mean   :2              Mean   :2.115   Mean   : 3.616e-10   Mean   :0.27266   Mean   :0.1873       Mean   :-0.9451   Mean   :2.6977   Mean   : 1.582e-08   Mean   :0.26126   Mean   :0.19298   Mean   :-0.8663   Mean   :2.4980   Mean   : 1.948e-10   Mean   :0.13268   Mean   :0.052156   Mean   :-0.7439   Mean   :2.2763   Mean   : 2.465e-10   Mean   :0.24376   Mean   :0.17949   Mean   :-0.7504   Mean   :2.4162   Mean   : 1380619     Mean   : 687332   Mean   :1301966      Mean   :0.3342   Mean   :-0.005629   Mean   :0.11401    Mean   :-0.14251    Mean   :0.19364    Mean   :0.08524        Mean   :0.3505        Mean   :-0.004234   Mean   :0.07604   Mean   :-0.1340   Mean   :0.1679   Mean   :0.08049   Mean   :0.3482   Mean   :-0.0025467   Mean   :0.11020   Mean   :-0.019512   Mean   :0.13075   Mean   : 0.01199    Mean   :0.14684    Mean   :-0.0030825   Mean   :0.04400   Mean   :-0.12299   Mean   :0.14756   Mean   :0.07325   Mean   :0.3311   Mean   :-0.28117                  Mean   :0.15388                  Mean   :-0.02598                  Mean   :0.13354                  Mean   :0.46239                   Mean   :-0.0048569                Mean   :0.11238                  Mean   :2.5              Mean   :7.1              Mean   :0.5             
     1dd7 HEM 600 A:  1   1of8   :  4   MG     : 15   3rd Qu.: 991.0   E      :  6   3rd Qu.:20.00          3rd Qu.:20.00                3rd Qu.:20.00                        3rd Qu.:146.0                       3rd Qu.:134.00                                3rd Qu.:10.000           3rd Qu.: 2.000           3rd Qu.: 5.000           3rd Qu.:0.0000           3rd Qu.:21.00           3rd Qu.:147.0                  3rd Qu.:10.000      3rd Qu.: 2.00       3rd Qu.: 6.000      3rd Qu.:0.0000      3rd Qu.: 39.109             3rd Qu.: 63.15            3rd Qu.:1.0000       3rd Qu.: 2484730   3rd Qu.:1.060e+12   3rd Qu.:9.290e+16   3rd Qu.:1.702e+14   3rd Qu.:0.8482          3rd Qu.:0.11688         3rd Qu.:0.003339        3rd Qu.:0.070190        3rd Qu.:1.139e+09   3rd Qu.:1.101e+17   3rd Qu.:6.898e+17   3rd Qu.:1.252e+14   3rd Qu.:4.794e+13   3rd Qu.:1.574e+15   3rd Qu.: 1.09148        3rd Qu.: 0.098239       3rd Qu.:  0.66626       3rd Qu.:0.040419        3rd Qu.:0.020802        3rd Qu.: 0.512024       3rd Qu.:0.003224          3rd Qu.:1.0e-06           3rd Qu.:0.000007          3rd Qu.:8.0e-06           3rd Qu.:0.000004          3rd Qu.:0e+00             3rd Qu.: 8538        3rd Qu.:0.26386       3rd Qu.:0.55046       3rd Qu.:0.71085       3rd Qu.:15.547          3rd Qu.: 7.292          3rd Qu.:4.196           3rd Qu.: 1489917     3rd Qu.:4.020e+11    3rd Qu.:2.119e+16    3rd Qu.:8.885e+13    3rd Qu.:1.15281           3rd Qu.:0.201483          3rd Qu.:0.007639          3rd Qu.:0.218451          3rd Qu.:6.616e+08    3rd Qu.:3.999e+16    3rd Qu.:2.059e+17    3rd Qu.:5.281e+13    3rd Qu.:2.560e+13    3rd Qu.:5.102e+14    3rd Qu.: 2.12660          3rd Qu.: 0.317839         3rd Qu.:  2.51551         3rd Qu.:0.139907          3rd Qu.:0.088392          3rd Qu.: 1.354014         3rd Qu.:0.009081            3rd Qu.:0.000007            3rd Qu.:0.000055            3rd Qu.:0.000040            3rd Qu.:0.000024            3rd Qu.:0.0e+00             3rd Qu.: 5393.2        3rd Qu.:0.27433         3rd Qu.:0.56778         3rd Qu.:0.71864         3rd Qu.:15.702            3rd Qu.: 6.949            3rd Qu.:3.910             3rd Qu.:1079.1   3rd Qu.: 40.505   3rd Qu.:0.05497   3rd Qu.:0.2135   3rd Qu.:0     3rd Qu.:2.0222   3rd Qu.:0.3490   3rd Qu.:1.0000   NA           NA            NA             3rd Qu.:0.2    3rd Qu.:1.9      3rd Qu.:1.4              3rd Qu.:2              3rd Qu.:2.350   3rd Qu.: 5.607e-10   3rd Qu.:0.32402   3rd Qu.:0.2380       3rd Qu.:-0.7756   3rd Qu.:3.2264   3rd Qu.: 4.987e-10   3rd Qu.:0.30827   3rd Qu.:0.24589   3rd Qu.:-0.6704   3rd Qu.:3.1065   3rd Qu.: 1.168e-10   3rd Qu.:0.16144   3rd Qu.:0.068014   3rd Qu.:-0.5552   3rd Qu.:3.1125   3rd Qu.: 5.154e-10   3rd Qu.:0.29076   3rd Qu.:0.24051   3rd Qu.:-0.5671   3rd Qu.:2.9495   3rd Qu.: 1594033     3rd Qu.: 802854   3rd Qu.:1431662      3rd Qu.:0.4394   3rd Qu.:-0.001685   3rd Qu.:0.14164    3rd Qu.:-0.11771    3rd Qu.:0.22669    3rd Qu.:0.09289        3rd Qu.:0.4093        3rd Qu.:-0.000850   3rd Qu.:0.09156   3rd Qu.:-0.1091   3rd Qu.:0.1946   3rd Qu.:0.08881   3rd Qu.:0.4092   3rd Qu.:-0.0004405   3rd Qu.:0.13646   3rd Qu.:-0.007422   3rd Qu.:0.15590   3rd Qu.: 0.01618    3rd Qu.:0.17728    3rd Qu.:-0.0004328   3rd Qu.:0.05351   3rd Qu.:-0.10593   3rd Qu.:0.16976   3rd Qu.:0.08319   3rd Qu.:0.4010   3rd Qu.:-0.23512                  3rd Qu.:0.18005                  3rd Qu.:-0.01745                  3rd Qu.:0.16339                  3rd Qu.:0.51481                   3rd Qu.:-0.0009203                3rd Qu.:0.14125                  3rd Qu.:2.5              3rd Qu.:7.1              3rd Qu.:0.5             
     1dud DUD 153 A:  1   2c6c   :  4   CL     : 12   Max.   :5171.0   F      :  4   Max.   :68.00          Max.   :68.00                Max.   :68.00                        Max.   :437.0                       Max.   :437.00                                Max.   :53.000           Max.   :11.000           Max.   :18.000           Max.   :2.0000           Max.   :68.00           Max.   :437.0                  Max.   :53.000      Max.   :11.00       Max.   :18.000      Max.   :2.0000      Max.   :152.236             Max.   :242.42            Max.   :2.0000       Max.   :32882595   Max.   :1.313e+14   Max.   :1.232e+20   Max.   :1.091e+17   Max.   :2.4196          Max.   :0.45516         Max.   :0.025738        Max.   :1.635149        Max.   :6.490e+10   Max.   :3.246e+20   Max.   :3.700e+21   Max.   :6.394e+16   Max.   :3.428e+16   Max.   :1.553e+18   Max.   :10.79379        Max.   :13.709319       Max.   :104.44385       Max.   :1.760100        Max.   :2.113039        Max.   :17.320131       Max.   :0.025201          Max.   :7.5e-05           Max.   :0.000542          Max.   :2.2e-04           Max.   :0.000213          Max.   :1e-06             Max.   :30303        Max.   :0.88871       Max.   :0.98585       Max.   :0.98273       Max.   :36.938          Max.   :14.183          Max.   :8.873           Max.   :16969306     Max.   :3.765e+13    Max.   :2.099e+19    Max.   :4.476e+16    Max.   :2.76633           Max.   :0.961646          Max.   :0.064973          Max.   :5.565394          Max.   :4.101e+10    Max.   :9.889e+19    Max.   :1.490e+21    Max.   :2.795e+16    Max.   :1.674e+16    Max.   :5.041e+17    Max.   :13.12065          Max.   :17.019327         Max.   :152.53080         Max.   :6.048011          Max.   :6.369755          Max.   :26.639321         Max.   :0.142323            Max.   :0.000979            Max.   :0.016789            Max.   :0.009153            Max.   :0.009207            Max.   :7.3e-05             Max.   :19029.5        Max.   :0.90248         Max.   :0.99128         Max.   :0.98031         Max.   :36.837            Max.   :13.965            Max.   :8.117             Max.   :7156.2   Max.   :156.792   Max.   :0.23878   Max.   :0.9538   Max.   :0     Max.   :8.3774   Max.   :1.6245   Max.   :2.0000   NA           NA            NA             Max.   :0.2    Max.   :1.9      Max.   :1.4              Max.   :2              Max.   :4.506   Max.   : 1.840e-08   Max.   :0.52631   Max.   :0.5806       Max.   :-0.4103   Max.   :7.8157   Max.   : 1.526e-06   Max.   :0.51134   Max.   :0.62257   Max.   :-0.4099   Max.   :8.4740   Max.   : 8.501e-09   Max.   :0.24496   Max.   :0.288558   Max.   :-0.2367   Max.   :7.4729   Max.   : 9.740e-09   Max.   :0.48014   Max.   :0.58475   Max.   :-0.3141   Max.   :8.4967   Max.   :11616364     Max.   :3979359   Max.   :9601808      Max.   :0.7041   Max.   : 0.015597   Max.   :0.22176    Max.   :-0.02865    Max.   :0.29957    Max.   :0.15396        Max.   :0.5877        Max.   : 0.010268   Max.   :0.16330   Max.   :-0.0428   Max.   :0.2519   Max.   :0.15248   Max.   :0.5897   Max.   : 0.0200325   Max.   :0.20029   Max.   : 0.015555   Max.   :0.22209   Max.   : 0.04065    Max.   :0.25360    Max.   : 0.0227987   Max.   :0.12305   Max.   :-0.02752   Max.   :0.22334   Max.   :0.13819   Max.   :0.5602   Max.   :-0.05475                  Max.   :0.30463                  Max.   : 0.05818                  Max.   :0.27808                  Max.   :0.92726                   Max.   : 0.0174395                Max.   :0.22160                  Max.   :2.5              Max.   :7.1              Max.   :0.5             
     (Other)       :387   (Other):365   (Other):261   NA               (Other): 16   NA                     NA                           NA                                   NA                                  NA                                            NA                       NA                       NA                       NA                       NA                      NA                             NA                  NA                  NA                  NA                  NA                          NA                        NA                   NA's   :29         NA's   :29          NA's   :29          NA's   :29          NA's   :29              NA's   :29              NA's   :29              NA's   :29              NA's   :29          NA's   :29          NA's   :29          NA's   :29          NA's   :29          NA's   :29          NA's   :29              NA's   :29              NA's   :29              NA's   :29              NA's   :29              NA's   :29              NA's   :29                NA's   :29                NA's   :29                NA's   :29                NA's   :29                NA's   :29                NA's   :29           NA's   :29            NA's   :29            NA's   :29            NA's   :29              NA's   :29              NA's   :29              NA's   :29           NA's   :29           NA's   :29           NA's   :29           NA's   :29                NA's   :29                NA's   :29                NA's   :29                NA's   :29           NA's   :29           NA's   :29           NA's   :29           NA's   :29           NA's   :29           NA's   :29                NA's   :29                NA's   :29                NA's   :29                NA's   :29                NA's   :29                NA's   :29                  NA's   :29                  NA's   :29                  NA's   :29                  NA's   :29                  NA's   :29                  NA's   :29             NA's   :29              NA's   :29              NA's   :29              NA's   :29                NA's   :29                NA's   :29                NA               NA                NA                NA               NA            NA               NA               NA               NA           NA            NA             NA             NA               NA                       NA                     NA              NA                   NA                NA                   NA                NA               NA                   NA                NA                NA                NA               NA                   NA                NA                 NA                NA               NA                   NA                NA                NA                NA               NA                   NA                NA                   NA               NA's   :2           NA's   :2          NA                  NA                 NA                     NA                    NA's   :2           NA's   :2         NA                NA               NA                NA               NA's   :2            NA's   :2         NA                  NA                NA                  NA                 NA's   :2            NA's   :2         NA                 NA                NA                NA               NA's   :2                         NA's   :2                        NA's   :2                         NA's   :2                        NA's   :2                         NA's   :2                         NA's   :2                        NA                       NA                       NA                      

### Korelacje pomiędzy zmiennymi

![](zed_files/figure-html/atomCountCorrelation-1.png)\![](zed_files/figure-html/atomCountCorrelation-2.png)\![](zed_files/figure-html/atomCountCorrelation-3.png)\![](zed_files/figure-html/atomCountCorrelation-4.png)\![](zed_files/figure-html/atomCountCorrelation-5.png)\![](zed_files/figure-html/atomCountCorrelation-6.png)\

### Liczność przykładów w każdej klasie


res_name    count
---------  ------
SO4            33
CA             27
GOL            23
ZN             22
MG             15
CL             12
NAG            12
EDO            10
PO4             9
HEM             8
FAD             7
ATP             5
CU              5
K               5
MN              5
NAD             5
NDP             5
ACY             4
ADP             4
EPE             3
FMN             3
GDP             3
NAP             3
NI              3
NLE             3
PEG             3
PLP             3
ACE             2
ACT             2
CD              2
CO              2
FE              2
IAS             2
MES             2
NDG             2
TRS             2
UMP             2
YCM             2
0B3             1
18F             1
1G5             1
24I             1
2B4             1
2D0             1
2JR             1
2NC             1
2YH             1
3MS             1
3PS             1
5HK             1
5P3             1
AAL             1
ABA             1
ADE             1
ADN             1
AKH             1
ASX             1
BA              1
BC2             1
BD6             1
BGC             1
BOG             1
BRU             1
BSA             1
CB3             1
CIT             1
CPS             1
CPT             1
CSO             1
CTO             1
CUA             1
CYC             1
D10             1
DCP             1
DEX             1
DHE             1
DHT             1
DIO             1
DKT             1
DLG             1
DOC             1
DUD             1
DUP             1
EBP             1
EDA             1
EOH             1
F89             1
FES             1
FLF             1
FMT             1
G3P             1
G75             1
GAI             1
GAL             1
GCP             1
GCS             1
GD              1
GLF             1
GTP             1
HDD             1
HEC             1
HEX             1
HG              1
HIC             1
HLT             1
IMD             1
IOD             1
IP2             1
IPA             1
IR4             1
JG1             1
K4F             1
KBI             1
LAT             1
LZP             1
M2T             1
M3I             1
MAN             1
ML2             1
MLI             1
MLT             1
MLY             1
MOO             1
MPD             1
MPO             1
MYR             1
N22             1
NGT             1
NH2             1
NJQ             1
NO3             1
NPW             1
OAR             1
OBN             1
OCS             1
OEU             1
OFM             1
OIL             1
P6G             1
PEP             1
PG0             1
PG4             1
PHS             1
PIN             1
PLG             1
PLM             1
PNW             1
PUT             1
PYR             1
RDC             1
RIE             1
SAH             1
SCN             1
SMT             1
SPX             1
STI             1
SUG             1
T23             1
TAM             1
THP             1
TPO             1
TYD             1
UD1             1
UDP             1
UTP             1
VAP             1
VDX             1
XE              1
ZYV             1

### Rozkład liczby atomów

![](zed_files/figure-html/atomDistribution-1.png)\

### Rozkład liczby elektronów

![](zed_files/figure-html/electronDistribution-1.png)\

### Próba odtworzenia wykresu

![](zed_files/figure-html/cloningAPlot-1.png)\

### 10 klas z największą niezgodnością liczby atomów

res_name    var
---------  ----
UTP          16
NAG          11
EPE          10
NDP           8
CPS           6
ATP           4
NLE           3
PLP           3
IAS           2
AAL           1
ABA           1
BGC           1
BRU           1
BSA           1
CPT           1
CSO           1
DOC           1
EDA           1
GAL           1
GCS           1
HIC           1
MAN           1
MLY           1
OCS           1
OFM           1
OIL           1
PYR           1
SMT           1
SO4           1
T23           1
YCM           1

### 10 klas z największą niezgodnością liczby elektronów


res_name    var
---------  ----
UTP         108
NAG          88
EPE          64
CPS          52
NDP          51
ATP          39
NLE          24
PLP          24
CPT          17
IAS          16

### Rozkład kolumn zaczynających się od part_01

![](zed_files/figure-html/part_01Distribution-1.png)\![](zed_files/figure-html/part_01Distribution-2.png)\![](zed_files/figure-html/part_01Distribution-3.png)\![](zed_files/figure-html/part_01Distribution-4.png)\![](zed_files/figure-html/part_01Distribution-5.png)\![](zed_files/figure-html/part_01Distribution-6.png)\![](zed_files/figure-html/part_01Distribution-7.png)\![](zed_files/figure-html/part_01Distribution-8.png)\![](zed_files/figure-html/part_01Distribution-9.png)\![](zed_files/figure-html/part_01Distribution-10.png)\![](zed_files/figure-html/part_01Distribution-11.png)\![](zed_files/figure-html/part_01Distribution-12.png)\![](zed_files/figure-html/part_01Distribution-13.png)\![](zed_files/figure-html/part_01Distribution-14.png)\![](zed_files/figure-html/part_01Distribution-15.png)\![](zed_files/figure-html/part_01Distribution-16.png)\![](zed_files/figure-html/part_01Distribution-17.png)\![](zed_files/figure-html/part_01Distribution-18.png)\![](zed_files/figure-html/part_01Distribution-19.png)\![](zed_files/figure-html/part_01Distribution-20.png)\![](zed_files/figure-html/part_01Distribution-21.png)\![](zed_files/figure-html/part_01Distribution-22.png)\![](zed_files/figure-html/part_01Distribution-23.png)\![](zed_files/figure-html/part_01Distribution-24.png)\![](zed_files/figure-html/part_01Distribution-25.png)\![](zed_files/figure-html/part_01Distribution-26.png)\![](zed_files/figure-html/part_01Distribution-27.png)\![](zed_files/figure-html/part_01Distribution-28.png)\![](zed_files/figure-html/part_01Distribution-29.png)\![](zed_files/figure-html/part_01Distribution-30.png)\![](zed_files/figure-html/part_01Distribution-31.png)\![](zed_files/figure-html/part_01Distribution-32.png)\![](zed_files/figure-html/part_01Distribution-33.png)\![](zed_files/figure-html/part_01Distribution-34.png)\![](zed_files/figure-html/part_01Distribution-35.png)\![](zed_files/figure-html/part_01Distribution-36.png)\![](zed_files/figure-html/part_01Distribution-37.png)\![](zed_files/figure-html/part_01Distribution-38.png)\![](zed_files/figure-html/part_01Distribution-39.png)\![](zed_files/figure-html/part_01Distribution-40.png)\![](zed_files/figure-html/part_01Distribution-41.png)\![](zed_files/figure-html/part_01Distribution-42.png)\![](zed_files/figure-html/part_01Distribution-43.png)\![](zed_files/figure-html/part_01Distribution-44.png)\![](zed_files/figure-html/part_01Distribution-45.png)\![](zed_files/figure-html/part_01Distribution-46.png)\![](zed_files/figure-html/part_01Distribution-47.png)\![](zed_files/figure-html/part_01Distribution-48.png)\![](zed_files/figure-html/part_01Distribution-49.png)\![](zed_files/figure-html/part_01Distribution-50.png)\![](zed_files/figure-html/part_01Distribution-51.png)\![](zed_files/figure-html/part_01Distribution-52.png)\![](zed_files/figure-html/part_01Distribution-53.png)\![](zed_files/figure-html/part_01Distribution-54.png)\![](zed_files/figure-html/part_01Distribution-55.png)\![](zed_files/figure-html/part_01Distribution-56.png)\![](zed_files/figure-html/part_01Distribution-57.png)\![](zed_files/figure-html/part_01Distribution-58.png)\![](zed_files/figure-html/part_01Distribution-59.png)\![](zed_files/figure-html/part_01Distribution-60.png)\![](zed_files/figure-html/part_01Distribution-61.png)\![](zed_files/figure-html/part_01Distribution-62.png)\![](zed_files/figure-html/part_01Distribution-63.png)\![](zed_files/figure-html/part_01Distribution-64.png)\![](zed_files/figure-html/part_01Distribution-65.png)\![](zed_files/figure-html/part_01Distribution-66.png)\![](zed_files/figure-html/part_01Distribution-67.png)\![](zed_files/figure-html/part_01Distribution-68.png)\![](zed_files/figure-html/part_01Distribution-69.png)\

### Przewidywanie liczby atomów

```r
dataForTraining <- data %>% dplyr::select(local_res_atom_non_h_count, dict_atom_non_h_count)

inTraining <- 
    createDataPartition(
        # atrybut do stratyfikacji
        y = dataForTraining$local_res_atom_non_h_count,
        # procent w zbiorze uczącym
        p = .75,
        # chcemy indeksy a nie listę
        list = FALSE)

training <- dataForTraining[inTraining,]
testing  <- dataForTraining[-inTraining,]

ctrl <- trainControl(
    # powtórzona ocena krzyżowa
    method = "repeatedcv",
    # liczba podziałów
    number = 2,
    # liczba powtórzeń
    repeats = 5)

fit <- train(local_res_atom_non_h_count ~ .,
             data = training,
             method = "qrf", # Domyślnie RMSE and R^2
             trControl = ctrl)

fit
```

```
## Quantile Random Forest 
## 
## 296 samples
##   3 predictor
## 
## No pre-processing
## Resampling: Cross-Validated (2 fold, repeated 5 times) 
## Summary of sample sizes: 148, 148, 149, 147, 147, 149, ... 
## Resampling results across tuning parameters:
## 
##   mtry  RMSE       Rsquared   RMSE SD    Rsquared SD
##     2   16.683297  0.3443911  0.5415127  0.276263138
##   191    1.947121  0.9844191  0.5565921  0.007249607
##   380    1.647592  0.9889265  0.3925885  0.004599976
## 
## RMSE was used to select the optimal model using  the smallest value.
## The final value used for the model was mtry = 380.
```

```r
rfClasses <- predict(fit, newdata = testing)
```

Dokładność: 0.9938475

### Przewidywanie liczby elektronów

```r
dataForTraining <- data %>% dplyr::select(local_res_atom_non_h_electron_sum, dict_atom_non_h_electron_sum)

inTraining <- 
    createDataPartition(
        # atrybut do stratyfikacji
        y = dataForTraining$local_res_atom_non_h_electron_sum,
        # procent w zbiorze uczącym
        p = .75,
        # chcemy indeksy a nie listę
        list = FALSE)

training <- dataForTraining[inTraining,]
testing  <- dataForTraining[-inTraining,]

ctrl <- trainControl(
    # powtórzona ocena krzyżowa
    method = "repeatedcv",
    # liczba podziałów
    number = 2,
    # liczba powtórzeń
    repeats = 5)

fit <- train(local_res_atom_non_h_electron_sum ~ .,
             data = training,
             method = "qrf", # Domyślnie RMSE and R^2
             trControl = ctrl)
```

```
## Warning in nominalTrainWorkflow(x = x, y = y, wts = weights, info =
## trainInfo, : There were missing values in resampled performance measures.
```

```r
fit
```

```
## Quantile Random Forest 
## 
## 296 samples
##   3 predictor
## 
## No pre-processing
## Resampling: Cross-Validated (2 fold, repeated 5 times) 
## Summary of sample sizes: 147, 149, 147, 149, 148, 148, ... 
## Resampling results across tuning parameters:
## 
##   mtry  RMSE       Rsquared   RMSE SD   Rsquared SD
##     2   110.64546  0.2141785  7.047381  0.208761404
##   191    13.07854  0.9845674  2.760465  0.005752021
##   380    12.10332  0.9863360  1.984673  0.004463231
## 
## RMSE was used to select the optimal model using  the smallest value.
## The final value used for the model was mtry = 380.
```

```r
rfClasses <- predict(fit, newdata = testing)
```

Dokładność: 0.9990057

### Klasyfikator

Z klasyfikatorem niestety jest problem taki, że czas jego generowania jest bardzo długi a po jakimś czasie rzuca błędem, poniżej jest próba jego napisania.


```r
dataForTraining <- data %>% dplyr::select(res_name, local_volume, local_electrons, local_mean, local_std, local_min, local_max, local_skewness, local_parts, solvent_mask_count, void_mask_count, modeled_mask_count, solvent_ratio)  %>% na.omit()

inTraining <- 
    createDataPartition(
        # atrybut do stratyfikacji
        y = dataForTraining$res_name,
        # procent w zbiorze uczącym
        p = .75,
        # chcemy indeksy a nie listę
        list = FALSE)

training <- dataForTraining[ inTraining,]
testing  <- dataForTraining[-inTraining,]

ctrl <- trainControl(
    # powtórzona ocena krzyżowa
    method = "repeatedcv",
    classProbs=TRUE,
    # liczba podziałów
    number = 2,
    # liczba powtórzeń
    repeats = 5)

fit <- train(res_name ~ .,
             data = training,
             method = "rf",
             trControl = ctrl,
             ntree = 10)

rfClasses <- predict(fit, newdata = testing)
confusionMatrix(data=rfClasses, testing$res_name)
```

